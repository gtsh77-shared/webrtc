module git.gbase.pw/tw/webrtc

go 1.17

require (
	git.gbase.pw/tw/interceptor v0.0.0-20220610224429-033ee5e8c35e
	git.gbase.pw/tw/rtcp v0.0.0-20220610223922-5b69f384dab5
	github.com/pion/datachannel v1.5.2
	github.com/pion/dtls/v2 v2.1.5
	github.com/pion/ice/v2 v2.2.6
	github.com/pion/logging v0.2.2
	github.com/pion/randutil v0.1.0
	github.com/pion/rtp v1.7.13
	github.com/pion/sctp v1.8.2
	github.com/pion/sdp/v3 v3.0.5
	github.com/pion/srtp/v2 v2.0.9
	github.com/pion/transport v0.13.0
	github.com/sclevine/agouti v3.0.0+incompatible
	github.com/stretchr/testify v1.7.2
	golang.org/x/net v0.0.0-20220607020251-c690dde0001d
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.19.0 // indirect
	github.com/pion/mdns v0.0.5 // indirect
	github.com/pion/rtcp v1.2.9 // indirect
	github.com/pion/stun v0.3.5 // indirect
	github.com/pion/turn/v2 v2.0.8 // indirect
	github.com/pion/udp v0.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20220427172511-eb4f295cb31f // indirect
	golang.org/x/sys v0.0.0-20220520151302-bc2c85ada10a // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
